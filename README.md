datalife_product_cost_manage
============================

The product_cost_manage module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-product_cost_manage/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-product_cost_manage)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
