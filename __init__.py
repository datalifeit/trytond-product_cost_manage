# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import category


def register():
    Pool.register(
        product.Template,
        product.Product,
        category.CostCategory,
        module='product_cost_manage', type_='model')
