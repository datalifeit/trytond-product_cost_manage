# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.modules.cost_manage.cost_manage import CategorizedMixin
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class Template(CategorizedMixin, metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        ProductCategory = pool.get('product.category')
        cursor = Transaction().connection.cursor()

        sql_table = cls.__table__()
        table_h = cls.__table_handler__(module_name)
        product_category = ProductCategory.__table__()

        cost_concept_category_exists = table_h.column_exist(
            'cost_concept_category')
        costs_category_exists = table_h.column_exist('costs_category')

        super().__register__(module_name)

        if cost_concept_category_exists and costs_category_exists:
            cursor.execute(*sql_table.update(
                columns=[sql_table.cost_category],
                values=[
                    product_category.select(
                        product_category.cost_category,
                        where=(product_category.id
                            == sql_table.cost_concept_category))],
                where=sql_table.costs_category))

            table_h.drop_column('cost_concept_category')
            table_h.drop_column('costs_category')


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    @property
    def cost_category_used(self):
        return self.template.cost_category_used
