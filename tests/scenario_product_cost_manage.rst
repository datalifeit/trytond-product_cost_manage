===================
Product Cost Manage
===================

Imports::

    >>> from decimal import Decimal
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import \
    ...     create_company, get_company


Install product_cost_manage::

    >>> config = activate_modules('product_cost_manage')


Get Models::

    >>> CostCategory = Model.get('cost.manage.category')
    >>> ModelData = Model.get('ir.model.data')
    >>> Productuom = Model.get('product.uom')
    >>> Template = Model.get('product.template')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create cost category::

    >>> data, = ModelData.find([
    ...        ('module', '=', 'product_cost_manage'),
    ...        ('fs_id', '=', 'cost_category_product'),
    ...        ], limit=1)
    >>> root_cost_category = CostCategory(data.db_id)

    >>> cost_category = CostCategory()
    >>> cost_category.name = 'Cost category 1'
    >>> cost_category.parent = root_cost_category
    >>> cost_category.save()


Get uom::

    >>> data, = ModelData.find([
    ...        ('module', '=', 'product'),
    ...        ('fs_id', '=', 'uom_unit')])
    >>> unit = Productuom(data.db_id)


Create Template::

    >>> template = Template()
    >>> template.name = 'Template Product'
    >>> template.list_price = Decimal(10)
    >>> template.cost_price = Decimal(3)
    >>> template.cost_category = cost_category
    >>> template.default_uom = unit
    >>> template.save()


Create cost category with paren in use::

    >>> cost_category2 = CostCategory()
    >>> cost_category2.name = 'Cost category 2'
    >>> cost_category2.parent = cost_category
    >>> cost_category2.save()
    Traceback (most recent call last):
      ...
    trytond.exceptions.UserError: You cannot define children for categories "Consumption / Cost category 1" because they are used in "Product Template". - 