# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import doctest
import unittest
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.pool import Pool
from trytond.tests.test_tryton import (ModuleTestCase, doctest_checker,
    doctest_teardown)
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import with_transaction


class ProductCostManageTestCase(ModuleTestCase):
    """Test Product Cost Manage module"""
    module = 'product_cost_manage'

    @with_transaction()
    def test_cost_category_used(self):
        'Create template with cost category'
        pool = Pool()
        Template = pool.get('product.template')
        CostCategory = Pool().get('cost.manage.category')
        ModelData = pool.get('ir.model.data')
        ProductUom = pool.get('product.uom')

        consumption = CostCategory(ModelData.get_id('product_cost_manage',
            'cost_category_product'))
        cost_category = CostCategory(name='Cost Manage 1', parent=consumption)
        cost_category.save()
        unit, = ProductUom.search([('name', '=', 'Unit')])
        template = Template(
                name='Template Product',
                list_price=Decimal(10),
                cost_price=Decimal(3),
                default_uom=unit.id,
                cost_category=cost_category
                )
        template.save()
        self.assertEqual(template.cost_category_used, cost_category)

    @with_transaction()
    def test_cost_category_used_costs_category(self):
        'Create template with costs category and product category'
        pool = Pool()
        Template = pool.get('product.template')
        CostCategory = pool.get('cost.manage.category')
        ProductCategory = pool.get('product.category')
        ProductUom = pool.get('product.uom')
        Product = pool.get('product.product')

        unit, = ProductUom.search([('name', '=', 'Unit')])
        root_cost_cat, = CostCategory.search([
            ('name', '=', 'Consumption')])
        cost_category = CostCategory(name='Cost 1', parent=root_cost_cat)
        cost_category.save()
        category1, = ProductCategory.create([{
                    'name': 'Product Category 1'}])
        template = Template(
                name='Template Product',
                list_price=Decimal(10),
                cost_price=Decimal(3),
                cost_category=cost_category.id,
                default_uom=unit.id)
        template.save()

        self.assertEqual(template.cost_category_used, cost_category)

        product = Product(template=template)
        product.save()

        self.assertEqual(product.cost_category_used, cost_category)

    @with_transaction()
    def test_cost_category_used_error(self):
        pool = Pool()
        Template = pool.get('product.template')
        ProductUom = pool.get('product.uom')
        Product = pool.get('product.product')

        unit, = ProductUom.search([('name', '=', 'Unit')])
        template = Template(
                name='Template Product',
                list_price=Decimal(10),
                cost_price=Decimal(3),
                default_uom=unit.id,
                )
        template.save()

        with self.assertRaises(UserError):
            self.assert_(template.cost_category_used)

        product = Product(template=template)
        product.save()

        with self.assertRaises(UserError):
            self.assert_(product.cost_category_used)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductCostManageTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_product_cost_manage.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
